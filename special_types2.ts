let w: unknown = 1;
w = "string";
w = {
    runANonExitentMethod: () =>{
        console.log("I think therefore I am");
    }
}as { runANonExitentMethod:()=> void}

if(typeof w === 'object' && w !== null) {
    (w as { runANonExitentMethod:()=> void}).runANonExitentMethod();
}